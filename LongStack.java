import java.util.LinkedList;
// interpret() suuresti ümber kirjutatud

/**
 * The class is destined for the homework assignment no 3 The skeleton is
 * attributed to jpoial. Src https://jpoial@bitbucket.org/i231/kodu3.git
 * 
 */
public class LongStack {

	/** magasin ise listina. */

	protected LinkedList<Long> lifo;
	private int size; // - oo õudust, see kirjutas standardmeetodi üle ;)

	public LongStack() {
		lifo = new LinkedList<Long>();
		size = 0;

	} // konstruktor

	// --------------------------------
	// malli võetud: http://enos.itcollege.ee/~ylari/I231/Intstack.java
	@Override
	public Object clone() throws CloneNotSupportedException {
		LongStack tmp = new LongStack();
		tmp.lifo = (LinkedList<Long>) lifo.clone();
		return tmp;
	}

	// ---------------------------------------
	public boolean stEmpty() {
		boolean empty = false;

		if (lifo.isEmpty()) {
			empty = true;
		}
		return empty;

	} // END of isEmpty

	// --------------------------------
	public int size() {
		return lifo.size();
	}

	// --------------------------------
	public void push(long a) {
		lifo.push(a);
		size++;
		return; // sest mulle meeldib nii, olgu siis void või mitte.
	}

	// --------------------------------
	public long pop() {

		long tagastus = 0L;
		tagastus = lifo.pop();
		size--;
		return tagastus;

	} // pop DONE

	public void op(String s) {

		// -----------------------------------
		String korraldus = s;
		if (korraldus.isEmpty()) {
			throw new RuntimeException("EXCEPTION at op: Tehe on puudu üldse");
			// ja väljalend
		}

		if (korraldus.length() > 1) {
			throw new RuntimeException(
					"EXCEPTION at op: MITMEmärgilist tehet (nagu log või sin) pole veel realiseeritud");
			// ja väljalend
		}
		char tehe = korraldus.charAt(0);
		if ((tehe != '+') && (tehe != '-') && (tehe != '*') && (tehe != '/')) {
			throw new RuntimeException(
					"EXCEPTION at op: Tehetest on lubatud vaid +-*/ ning see: "
							+ s + " küll lubatud ei ole.");
			// ja väljalend
		}
		// Siitmaalt arvutused alles
		long operand1 = lifo.remove(1);
		long operand2 = lifo.remove(0);
		long result = 0;
		if (tehe == '+') {
			result = operand1 + operand2;
		}
		if (tehe == '-') {
			result = operand1 - operand2;
		}
		if (tehe == '*') {
			result = operand1 * operand2;
		}
		if (tehe == '/') {
			result = operand1 / operand2;
		}
		lifo.push(result);
	} // Endof op

	// --------------------------------------
	public long tos() {
		long tagastus = 0L;
		tagastus = lifo.peek();
		return tagastus;
	}

	// ---------------------------------------
	@Override
	public boolean equals(Object o) {

		if (o instanceof LongStack) {
			LinkedList<Long> tmp = (LinkedList<Long>) ((LongStack) o).lifo;
			if (lifo.size() == tmp.size()) {
				for (int i = 0; i < lifo.size(); i++)
					if (lifo.get(i) != tmp.get(i))
						return false;
				return true;
			}
		}
		return false;

	}

	// ----------------------------------------
	@Override
	public String toString() {
		int suurus = lifo.size();
		StringBuilder koosteLiin = new StringBuilder();
		String resultaat;
		Long lisatav;
		for (int i = (suurus - 1); i >= 0; i--) {
			lisatav = lifo.get(i);
			koosteLiin.append(lisatav);
			if (i != 0) // v.a. viimane kord
				koosteLiin.append(" ");
		}
		resultaat = koosteLiin.toString();
		return resultaat;
	}

	// ----------------------------------------------------------
	/**
	 * Sisse antakse string, kus on avaldis, tagastatakse LONG arvväärtus
	 * tulemusega või karjutakse appi, kui ei õnnestunud.
	 * 
	 * @param pol
	 * @return
	 */
	public static long interpret(String pol) {

		String rpnInputString = pol;
		int pikkus = rpnInputString.length();
		System.out.println();
		System.out.print("Sisend: |" + rpnInputString + "| ;  ");

		if ((pikkus == 0) || rpnInputString == null) {
			System.out.println(" ERROR: tyhi ylesanne.");
			throw new RuntimeException(
					"EXCEPTION in interpret: ei ole ilus tühja ülesannet sisestada");
		}
		// Väga huvitav, et need mõlemad peab ühte tingimusse kirjutama,
		// kaht järjestikust IF'i pole võimalik, tekib dead code. Weird.

		LongStack sygavKaev = new LongStack();

		rpnInputString = rpnInputString.trim(); // arvatavasti pole vaja
		String[] parsedTokens = rpnInputString.trim().split("\\s+");
		// tundub, et testid lisatyhikuid sees ei kontrolli. Aga las olla.

		// System.out.print("Splitted: ");
		// for (String token : parsedTokens) {
		// System.out.print("|" + token);
		// }
		// System.out.println("|");

		int wordCount = parsedTokens.length;
		System.out.println("wordCount: " + wordCount + ".");
		int arguments = 0;
		int operators = 0;

		for (String token : parsedTokens) {
			System.out.print(" |" + token + "| = ");
			// ----- 1 -----------------
			// Hüpotees nr 1 - sisendis on number, lisame patta
			if (isNumeric(token)) {
				try {
					sygavKaev.push(Long.valueOf(token));
					arguments = arguments + 1;
					// System.out.print("NUMBER. ->");
				} catch (NumberFormatException e) {
					// Do Nothing, mask the exception
				}

			} else {

				// ------ 2 -------------
				String tehe = token;
				System.out.print(" ...");
				if (!isAcceptedSign((tehe))) {
					System.out.println(" ERROR: Keelatud tehtemärk: " + tehe);
					throw new RuntimeException(

							"EXCEPTION in interpret phase 2: Number see pole ja tehe (+-*/) ka mitte: |"
									+ tehe
									+ "| Kes kurat selle siia pani? BAILOUT!");
					// ja väljalend
				}

				// ----- 3 -------------
				if (sygavKaev.stEmpty()) {
					System.out.println(" ERROR: Kaev on tyhi!");
					throw new RuntimeException(
							"EXCEPTION in interpret phase 3: tehtemärk "
									+ tehe
									+ "on OK aga kaevus on 0 numbrit millega tehteid teha");
				} else {
					// ------ 4 ------------------
					if (sygavKaev.size() < 2) {
						System.out.println(" ERROR: Kaevus numbreid vähe!");
						throw new RuntimeException(
								"EXCEPTION in interpret phase 4: kaevus on vaid 1 element ja sellega teatavasti tehet teha ei saa.");
					} // BLOKK 4
				} // BLOKK 3

				sygavKaev.op(token);
				operators = operators + 1;

			} // endIF

		} // endFOR
		System.out.println();
		System.out.print("Done with Input: arguments=" + arguments + ", operators=" + operators);

		if ((arguments - 1) != operators) {
			System.out.println("  ERROR: arg-1 must eq op!");
			throw new RuntimeException(
					"EXCEPTION at interpret final phase - Stack is out of balance (integers: "
							+ arguments + ", operators: " + operators + ").");
		} else {
			System.out.println();
		} 
		return sygavKaev.pop();
	} // Endof interpret

	// --------------------------------------
	public static boolean isNumeric(String str) {
		// http://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-a-numeric-type-in-java
		// return str.matches("[+-]?\\d*(\\.\\d+)?");

		// EKSOLE, tegelikult LONG numbris komasid pole, aga siiski.
		// http://stackoverflow.com/questions/3507162/most-elegant-isnumeric-solution-for-java
		if (str.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) {
			System.out.print("numeric");
			return true;
		} else {
			System.out.print("notnum");
			return false;
		}
	}

	// --------------------------------------
	public static boolean isAcceptedSign(String str) {
		char tehe = str.charAt(0);
		System.out.print(" |" + tehe + "| ");
		if ((tehe == '+') || (tehe == '-') || (tehe == '*') || (tehe == '/')) {
			System.out.print("signOK; ");
			return true;
		}
		System.out.print(" signBS; ");
		return false;
	}

	// ===========================================================================
	public static void main(String[] argum) {
		// igaks juhuks erinevad nimed globaalsele ja lokaalsele listidele

		LongStack test = new LongStack();
		System.out.println("trace 01: " + test.size + " | " + test.toString());
		test.push(17L);
		System.out.println("trace 02: " + test.size + " | " + test.toString());
		test.push(53L);
		System.out.println("trace 03: " + test.size + " | " + test.toString());
		test.push(66L);
		System.out.println("trace 04: " + test.size + " | " + test.toString());
		test.push(24L);
		System.out.println("trace 05: " + test.size + " | " + test.toString());
		test.op("/");
		System.out.println("trace 06: " + test.size + " | " + test.toString());
		test.op("*");
		System.out.println("trace 07: " + test.size + " | " + test.toString());
		test.op("-");
		System.out.println("trace 08: " + test.size + " | " + test.toString());
		long tulemus = test.pop();
		System.out.println("trace 01: " + test.size + " | " + test.toString());

		System.out.println("tulemus A: " + tulemus);

		LongStack koopia = test;
		System.out.println(koopia.equals(test));
		System.out.println(test);
		System.out.println(koopia);
		try {
			koopia = (LongStack) test.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(koopia.equals(test));
		System.out.println(test);
		System.out.println(koopia);
		test.push(6);
		koopia.push(3);
		System.out.println(koopia.equals(test));
		System.out.println(test);
		System.out.println(koopia);
		test.pop();
		System.out.println(koopia.equals(test));
		System.out.println(test);
		System.out.println(koopia);
	}
}

