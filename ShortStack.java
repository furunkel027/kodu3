import java.util.LinkedList;

public class ShortStack {


	protected LinkedList<Long> lifo;
	private int size;

	public ShortStack() {
		lifo = new LinkedList<Long>();
		size = 0;
	} // konstruktor

	// ------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		ShortStack tmp = new ShortStack();
		int blaah = lifo.size();
		tmp.lifo = (LinkedList<Long>) lifo.clone();
		return tmp;
	}

	// ------------------------------
	public boolean stEmpty() {
		boolean empty = false;
		if (lifo.isEmpty()) {
			empty = true;
		}
		return empty;
	} 

	// ------------------------------
	
	public int size() {

		return lifo.size();
	}

	// -------------------------------
	public void push(long a) {
	
		lifo.push(a);
		size++;
		return;
	}
	// -------------------------------
	public long pop() {

		long tagastus = 0L;
		tagastus = lifo.pop();
		size--;
		return tagastus;
	} 
	// --------------------------------
	public void op(String s) {

		String korraldus = s;
		if (korraldus.isEmpty()) {
			throw new RuntimeException("EXCEPTION at op: Tehe on puudu üldse");
			// ja väljalend
		}

		if (korraldus.length() > 1) {
			throw new RuntimeException(
					"EXCEPTION at op: MITMEmärgilist tehet (nagu log või sin) pole veel realiseeritud");
			// ja väljalend
		}
		char tehe = korraldus.charAt(0);
		if ((tehe != '+') && (tehe != '-') && (tehe != '*') && (tehe != '/')) {
			throw new RuntimeException(
					"EXCEPTION at op: Tehetest on lubatud vaid +-*/ ning see: " + s
							+ " küll lubatud ei ole.");
			// ja väljalend
		}
		
		// Siitmaalt arvutused alles
		long operand1 = lifo.remove(1);
		long operand2 = lifo.remove(0);
		long result = 0;
		if (tehe == '+') {
			result = operand1 + operand2;
		}
		if (tehe == '-') {
			result = operand1 - operand2;
		}
		if (tehe == '*') {
			result = operand1 * operand2;
		}
		if (tehe == '/') {
			result = operand1 / operand2;
		}
		lifo.push(result);
	}

	public long tos() {
		long tagastus = 0L;
		tagastus = lifo.peek();
		return tagastus;
	}
	
	// -----------------------------------
	@Override
	public boolean equals(Object o) {

		if (o instanceof ShortStack) {
			LinkedList<Long> tmp = (LinkedList<Long>) ((ShortStack) o).lifo;
			if (lifo.size() == tmp.size()) {
				for (int i = 0; i < lifo.size(); i++)
					if (lifo.get(i) != tmp.get(i))
						return false;
				return true;
			}
		}
		return false;

	}

	// --------------------------------------
	@Override
	public String toString() {
		int suurus = lifo.size();
		StringBuilder koosteLiin = new StringBuilder();
		String resultaat;
		Long lisatav;
		for (int i = (suurus - 1); i >= 0; i--) {
			lisatav = lifo.get(i);
			koosteLiin.append(lisatav);
			if (i != 0) // v.a. viimane kord
				koosteLiin.append(" ");
		}
		resultaat = koosteLiin.toString();
		return resultaat;
	}

	// --------------------
	public static long interpret(String pol) {
		String rpnInputString = pol;
		int pikkus = rpnInputString.length();

		if ((pikkus == 0) || rpnInputString == null) {
			throw new RuntimeException(
					"EXCEPTION in interpret: ei ole ilus tühja ülesannet sisestada");
		}
		// Väga huvitav, et need mõlemad peab ühte tingimusse kirjutama,
		// kaht järjestikust IF'i pole võimalik, tekib dead code. Weird.


		ShortStack sygavKaev = new ShortStack();

		String[] parsedTokens = rpnInputString.trim().split("\\w+");
		// int wordCount = parsedTokens.length;
		int arguments = 0;
		int operators = 0;
		for (String token : parsedTokens) {

			// ----- 1 -----------------
			// Hüpotees nr 1 - on number, lisame patta
			if (isNumeric(token)) {
				try {
					sygavKaev.push(Long.valueOf(token));
					arguments = arguments + 1;
				} catch (NumberFormatException e) {

					// ------ 2 -------------
					String tehe = token;

					if ((tehe != "+") && (tehe != "-") && (tehe != "*")
							&& (tehe != "/")) {
						throw new RuntimeException(
								"EXCEPTION in interpret phase 2: Number see pole ja tehe (+-*/) ka mitte: |"
										+ tehe
										+ "| Kes kurat selle siia pani? BAILOUT!");
						// ja väljalend
					} else {
						// ----- 3 -------------
						if (sygavKaev.stEmpty()) {
							throw new RuntimeException(
									"EXCEPTION in interpret phase 3: tehtemärk "
											+ tehe
											+ "ei saa no kuidagi hakata RPN kaevu ainsaks sümboliks.");
						} else {
							// ------ 4 ------------------
							if (sygavKaev.size() < 2) {
								throw new RuntimeException(
										"EXCEPTION in interpret phase 4: kaevus on vaid 1 element ja sellega teatavasti tehet teha ei saa.");
							} // BLOKK 4
						} // BLOKK 3
					} // BLOKK 2
					sygavKaev.op(token);
					operators = operators + 1;
				} // CATCH
			} // endIF
		} // endFOR

		
		if ((arguments - 1) != operators) 
			throw new RuntimeException(
					"EXCEPTION at interpret final phase - Stack is out of balance (integers: "
							+ arguments + ", operators: " + operators + ").");
		
		return sygavKaev.pop();
	}

	// public static void printout(LongStack t) {
	// {

	public static boolean isNumeric(String str) {
		if (str.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+"))
			return true;
		return false;
	}

	public static void main(String[] argum) {

		long tulemus = 0;
		String test011 = "35 3 +" ;
		tulemus = interpret(test011);
	}
	// Position error !!!

}

// # ---------------------------------------------------
// Exception in thread "main" java.lang.RuntimeException: 
//       EXCEPTION at interpret final phase - Stack is out of balance (integers: 0, operators: 0).
// at ShortStack.interpret(ShortStack.java:197)
// at ShortStack.main(ShortStack.java:217)

// Vead:

// testInterpretLong(LongStackTest)
// java.lang.NumberFormatException: For input string: ""
//
// testInterpretTokenizer(LongStackTest)
// java.lang.NumberFormatException: For input string: ""
//
// testInterpret(LongStackTest)
// java.util.NoSuchElementException: null


// Uue katse vead

//testInterpret(LongStackTest)
//java.lang.RuntimeException: EXCEPTION at pol final phase - Stack is out of balance (integers: 0, operators: 0).  
//
//testInterpretLong(LongStackTest)
//  java.lang.RuntimeException: EXCEPTION: Tehe on puudu üldse          
//
//testInterpretTokenizer(LongStackTest)
//  java.lang.RuntimeException: EXCEPTION: Tehe on puudu üldse          
//
//You could get 17 / 20 marks for this submission.


